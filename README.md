# Docker Library

Copyright 2019, Geeks Accelerator 
twins@geeksaccelerator.com


## Description

This is the Git repo of docker images maintained by Geeks Accelerator. 


## Push updates

```bash
docker login registry.gitlab.com
docker build -t golang1.13-docker -t registry.gitlab.com/geeks-accelerator/oss/docker-library:golang1.13-docker golang/1.13/docker
docker push registry.gitlab.com/geeks-accelerator/oss/docker-library:golang1.13-docker
```

## Join us on Gopher Slack

If you are having problems installing, troubles getting the project running or would like to contribute, join the channel #saas-starter-kit on [Gopher Slack](http://invite.slack.golangbridge.org/) 
